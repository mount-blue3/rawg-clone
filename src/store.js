import {configureStore} from '@reduxjs/toolkit';
import gamesReducer from "./sliceFilesFolder/gamesSlice";
import gameDetailsReducer from "./sliceFilesFolder/detailedGamePageSlice";

const store = configureStore({
  reducer: {
    games: gamesReducer,
    gameDetails: gameDetailsReducer,
  },
});
export default store;