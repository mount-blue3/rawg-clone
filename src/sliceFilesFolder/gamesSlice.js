import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  games: [],
  isLoading: true,
  isError: null,
  searchText: "",
  genre: "",
  platform: [],
  parentPlatforms: [],
  genresData: [],
  orderBy: "",
  selectedReleaseDate: "",
  page: 1,
  searchedGames: [],
  filteredByGenreAndOrder: [],
};

export const fetchGames = (page) => async (dispatch) => {
  try {
    const gamesData = await axios.get(
      `https://api.rawg.io/api/games?key=984f62ce408044e0a1065456e3bd0699&page=${page}`
    );
    const platformsData = await axios.get(
      `https://api.rawg.io/api/platforms/lists/parents?key=984f62ce408044e0a1065456e3bd0699`
    );
    const genresData = await axios.get(
      `https://api.rawg.io/api/genres?key=984f62ce408044e0a1065456e3bd0699`
    );
    const results = [gamesData, platformsData, genresData];
    const setResults = [setGames, setParentPlatforms, setGenresData];
    results.forEach((item, index) => {
      if (Array.isArray(item.data.results) && item.data.results.length >= 1) {
        // console.log(item.data.results);
        dispatch(setResults[index](item.data.results));
      } else {
        dispatch(setIsError("Data not found."));
      }
    });
    dispatch(setPage(page + 1));
  } catch (error) {
    dispatch(setIsError(`Failed to fetch data. Error: ${error.message}`));
  } finally {
    dispatch(setIsLoading(false));
  }
};

export const searchGames = (searchText) => async (dispatch) => {
  try {
    const response = await axios.get(
      `https://api.rawg.io/api/games?key=984f62ce408044e0a1065456e3bd0699&search=${searchText}&page_size=60`
    );
    // console.log("response from search", response.data.results);
    if (Array.isArray(response.data.results)) {
      dispatch(setSearchedGames(response.data.results));
    } else {
      dispatch(setIsError("Data not found."));
    }
  } catch (error) {
    dispatch(setIsError(`Failed to fetch data. Error: ${error.message}`));
  } finally {
    dispatch(setIsLoading(false));
  }
};

export const gamesFilteredByGenreAndOrder = (genre, orderby) => async (dispatch) => {
  try {
    const response = await axios.get(
      `https://api.rawg.io/api/games?key=984f62ce408044e0a1065456e3bd0699&genres=${genre}&ordering=${orderby}&page_size=40`
    );
    console.log("genresdata", response.data.results);
    dispatch(setFilteredByGenreAndOrder(response.data.results));
  } catch (error) {
    dispatch(setIsError(`Failed to fetch data. Error: ${error.message}`));
  } finally {
    dispatch(setIsLoading(false));
  }
};

const gamesSlice = createSlice({
  name: "games",
  initialState,
  reducers: {
    setGames: (state, action) => {
      state.games = state.games.concat(action.payload);
    },
    setSearchedGames: (state, action) => {
      state.searchedGames = action.payload;
    },
    setIsLoading: (state, action) => {
      state.isLoading = action.payload;
    },
    setIsError: (state, action) => {
      state.isError = action.payload;
    },
    setSearchText: (state, action) => {
      state.searchText = action.payload;
    },
    setGenre: (state, action) => {
      state.genre = action.payload;
    },
    setGenresData: (state, action) => {
      state.genresData = action.payload;
    },
    setPlatform: (state, action) => {
      state.platform = action.payload;
    },
    setParentPlatforms: (state, action) => {
      state.parentPlatforms = action.payload;
    },
    setOrderBy: (state, action) => {
      state.orderBy = action.payload;
    },
    setSelectedReleaseDate: (state, action) => {
      state.selectedReleaseDate = action.payload;
    },
    setPage: (state, action) => {
      state.page = action.payload;
    },
    setFilteredByGenreAndOrder: (state, action) => {
      state.filteredByGenreAndOrder = action.payload;
    },
  },
});

export const {
  setGames,
  setSearchedGames,
  setIsLoading,
  setIsError,
  setSearchText,
  setGenre,
  setGenresData,
  setPlatform,
  setParentPlatforms,
  setOrderBy,
  setSelectedReleaseDate,
  setPage,
  setFilteredByGenreAndOrder,
} = gamesSlice.actions;
export default gamesSlice.reducer;
