import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { setIsError } from "../sliceFilesFolder/gamesSlice";

const initialState = {
  gameDetails: {},
  gameSeries: [],
  dlcsAndEditions: [],
};

export const fetchGameDetails = (gameId) => async (dispatch) => {
  try {
    const gameDetails = await axios.get(
      `https://api.rawg.io/api/games/${gameId}?key=984f62ce408044e0a1065456e3bd0699`
    );
    const seriesOfGames = await axios.get(
      `https://api.rawg.io/api/games/${gameId}/game-series?key=984f62ce408044e0a1065456e3bd0699`
    );
    const dlcsAndEditions = await axios.get(
      `https://api.rawg.io/api/games/${gameId}/additions?key=984f62ce408044e0a1065456e3bd0699`
    );

    const results = [seriesOfGames, dlcsAndEditions];
    const setResults = [setGameSeries, setDlcsAndEditions];

    if (gameDetails.data) {
      dispatch(setGameDetails(gameDetails.data));
    } else {
      dispatch(setIsError("Data not found"));
    }

    results.forEach((item, index) => {
      if (Array.isArray(item.data.results) && item.data.results.length >= 1) {
        // console.log(item.data.results);
        dispatch(setResults[index](item.data.results));
      } else {
        dispatch(setIsError("Data not found"));
      }
    });

    // console.log("detailedgame", gameDetails.data);
    // console.log(seriesOfGames.data.results);
    // console.log(dlcsAndEditions.data.results);
  } catch (error) {
    dispatch(setIsError(`Failed to fetch data. Error: ${error.message}`));
  }
};

const detailedGamePageSlice = createSlice({
  name: "gameDetails",
  initialState,
  reducers: {
    setGameDetails: (state, action) => {
      state.gameDetails = action.payload;
    },
    setGameSeries: (state, action) => {
      state.gameSeries = action.payload;
    },
    setDlcsAndEditions: (state, action) => {
      state.dlcsAndEditions = action.payload;
    },
  },
});

export const { setGameDetails, setGameSeries, setDlcsAndEditions } =
  detailedGamePageSlice.actions;
export default detailedGamePageSlice.reducer;
