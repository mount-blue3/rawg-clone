import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter as Router } from "react-router-dom";
import { ChakraProvider, CSSReset } from '@chakra-ui/react'
import {Provider} from 'react-redux';
import store from './store.js'
import App from './App.jsx'
import './index.css'

ReactDOM.createRoot(document.getElementById("root")).render(
  <ChakraProvider>
    <CSSReset />
    <React.StrictMode>
      <Provider store={store}>
        <Router>
          <App />
        </Router>
      </Provider>
    </React.StrictMode>
  </ChakraProvider>
);
