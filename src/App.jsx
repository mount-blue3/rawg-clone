import React from "react";
import { Box } from "@chakra-ui/react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import Games from "./components/Games";
import Sidebar from "./components/Sidebar";
import "./App.css";
import DetailedGamePage from "./components/DetailedGamePage";

function App() {
  return (
    <>
      <Header />
      <Sidebar />
      <Routes>
        <Route path="/" element={<Games />} />
        <Route
          path="/games/detailed-page/:gameName/:gameID"
          element={<DetailedGamePage />}
        />
      </Routes>
    </>
  );
}

export default App;
