import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setPlatform } from "../sliceFilesFolder/gamesSlice";
import {
  Box,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuDivider,
} from "@chakra-ui/react";
import { CheckIcon, ChevronRightIcon } from "@chakra-ui/icons";

const FilterByPlatform = () => {
  const dispatch = useDispatch();
  const [hoveredPlatform, setHoveredPlatform] = useState(null);
  const selectedPlatform = useSelector((state) => state.games.platform);
  const parentPlatforms = useSelector((state) => state.games.parentPlatforms);
  const parentPlatformNames = parentPlatforms.map((item) => item.name);

  const platforms = [
    {
      name: "Apple Macintosh",
      platforms: ["macOS", "Classic Macintosh", "Apple II"],
    },
    { name: "Xbox", platforms: ["Xbox One", "Xbox Series S/X", "Xbox 360"] },
    {
      name: "PlayStation",
      platforms: [
        "PlayStation 5",
        "PlayStation 4",
        "PlayStation 3",
        "PlayStation 2",
        "Ps Vita",
        "PSP",
      ],
    },
    {
      name: "Nintendo",
      platforms: [
        "Nintendo 3Ds",
        "Nintendo Ds",
        "Nintendo Dsi",
        "Wii U",
        "Wii",
        "GameCube",
        "SNES",
        "NES",
      ],
    },
  ];

  const handlePlatformClick = (isSelectAll, value, parentPlatform) => {
    if (isSelectAll) {
      dispatch(setPlatform([isSelectAll, parentPlatform]));
    } else {
      dispatch(setPlatform([isSelectAll, value]));
    }
  };

  const handlePlatformMouseEnter = (platform) => {
    setHoveredPlatform(platform);
  };

  const handleClear = () => {
    dispatch(setPlatform([]));
  };

  const renderPlatformMenu = (platforms, parentPlatform) => {
    if (!platforms || platforms.length === 0) return null;

    return (
      <MenuList
        position="absolute"
        left="120px"
        top="60px"
        minWidth="160px"
        backgroundColor="white"
        boxShadow="md"
        py="2"
      >
        {platforms.map((platform) => (
          <MenuItem
            key={platform}
            onClick={() => handlePlatformClick(false, platform.toLowerCase())}
          >
            {platform}{" "}
            {selectedPlatform[1] === platform.toLowerCase() && (
              <CheckIcon color="green" ml={2} />
            )}
          </MenuItem>
        ))}
        <MenuDivider />
        <MenuItem
          color="green"
          onClick={() =>
            handlePlatformClick(
              true,
              parentPlatform.toLowerCase(),
              parentPlatform.toLowerCase()
            )
          }
        >
          Select all
        </MenuItem>
      </MenuList>
    );
  };

  return (
    <Box>
      <Menu>
        <MenuButton
          color="white"
          border="1px solid white"
          borderRadius="md"
          p="2"
        >
          {selectedPlatform[1] || "Platforms"}
        </MenuButton>
        <MenuList
          position="absolute"
          top="-50px"
          minWidth="160px"
          backgroundColor="white"
          boxShadow="md"
          py="2"
        >
          <MenuItem onClick={() => handlePlatformClick(false, "platforms")}>
            Platforms
          </MenuItem>
          {selectedPlatform[1] && (
            <>
              <MenuItem color="red" onClick={handleClear}>
                Clear
              </MenuItem>
              <MenuDivider />
            </>
          )}
          {parentPlatformNames.map((platformName, index) => {
            const matchingPlatforms = platforms.find(
              (parentPlatform) => parentPlatform.name === platformName
            );
            return (
              <React.Fragment key={index}>
                <MenuItem
                  onClick={() =>
                    handlePlatformClick(false, platformName.toLowerCase())
                  }
                  onMouseEnter={() => handlePlatformMouseEnter(platformName)}
                >
                  {platformName}{" "}
                  {selectedPlatform[1] === platformName.toLowerCase() && (
                    <CheckIcon color="green" ml={2} />
                  )}
                  {matchingPlatforms && <ChevronRightIcon />}
                </MenuItem>
                {matchingPlatforms &&
                  (hoveredPlatform === platformName
                    ? renderPlatformMenu(
                        matchingPlatforms.platforms,
                        platformName
                      )
                    : null)}
              </React.Fragment>
            );
          })}
        </MenuList>
      </Menu>
    </Box>
  );
};

export default FilterByPlatform;
