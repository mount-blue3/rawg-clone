import React from "react";
import {
  FaGamepad,
  FaWindows,
  FaXbox,
  FaPlaystation,
  FaApple,
  FaAndroid,
  FaLinux,
  FaChrome,
  FaGift,
  FaAppleAlt,
} from "react-icons/fa";
import { Box, Icon } from "@chakra-ui/react";
import { useSelector } from "react-redux";

const Platforms = () => {
  const games = useSelector((state) => state.games.games);
  const hoveredCardIndex = useSelector((state) => state.games.hoveredCardIndex);

  return (
    <Box mt="-2rem">
      {games &&
        games.map((game, gameIndex) => (
          <Box key={`${game.id}-${gameIndex}`}>
            {game.parent_platforms.map((platform) => {
              let PlatformIcon = null;
              switch (platform.platform.name) {
                case "PC":
                  PlatformIcon = FaWindows;
                  break;
                case "Xbox":
                  PlatformIcon = FaXbox;
                  break;
                case "PlayStation":
                  PlatformIcon = FaPlaystation;
                  break;
                case "Apple Macintosh":
                  PlatformIcon = FaAppleAlt;
                  break;
                case "iOS":
                  PlatformIcon = FaApple;
                  break;
                case "Android":
                  PlatformIcon = FaAndroid;
                  break;
                case "Linux":
                  PlatformIcon = FaLinux;
                  break;
                case "Web":
                  PlatformIcon = FaChrome;
                  break;
                case "Nintendo":
                  PlatformIcon = FaGamepad;
                  break;
                default:
                  PlatformIcon = FaGift;
                  break;
              }
              return (
                hoveredCardIndex === gameIndex &&
                PlatformIcon && (
                  <Icon
                    key={`${game.id}-${gameIndex}-${platform.platform.name}`}
                    as={PlatformIcon}
                    color="white"
                    boxSize={6}
                    mr={2}
                  />
                )
              );
            })}
          </Box>
        ))}
    </Box>
  );
};

export default Platforms;
