import React, { useState, useEffect } from "react";
import { FaGift } from "react-icons/fa";
import { Link, useNavigate } from "react-router-dom";
import { BiDotsHorizontalRounded } from "react-icons/bi";
import {
  Flex,
  Box,
  Card,
  Image,
  Heading,
  Button,
  Text,
  Divider,
  Container,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchGames,
  searchGames,
  gamesFilteredByGenreAndOrder,
} from "../sliceFilesFolder/gamesSlice";
import Platforms from "./Platforms";
import FilterByGenre from "./FilterByGenre";
import FilterByPlatform from "./FilterByPlatforms";
import OrderBy from "./OrderBy";
import ReleaseDate from "./ReleaseDate";

const Games = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [displayStyle, setDisplayStyle] = useState(true);
  const [hoveredCardIndex, setHoveredCardIndex] = useState(null);

  const games = useSelector((state) => state.games.games);
  const isLoading = useSelector((state) => state.games.isLoading);
  const isError = useSelector((state) => state.games.isError);
  const searchText = useSelector((state) => state.games.searchText);
  const genre = useSelector((state) => state.games.genre);
  const platform = useSelector((state) => state.games.platform);
  const orderBy = useSelector((state) => state.games.orderBy);
  const page = useSelector((state) => state.games.page);
  const selectedReleaseDate = useSelector(
    (state) => state.games.selectedReleaseDate
  );
  const searchedGames = useSelector((state) => state.games.searchedGames);
  const gamesByGenreAndOrder = useSelector(
    (state) => state.games.filteredByGenreAndOrder
  );

  useEffect(() => {
    dispatch(fetchGames(page));
  }, [page]);

  useEffect(() => {
    dispatch(searchGames(searchText));
  }, [searchText]);

  useEffect(() => {
    dispatch(gamesFilteredByGenreAndOrder(genre, orderBy));
  }, [genre, orderBy]);

  useEffect(() => {
    const params = new URLSearchParams();
    let hasParams = false;
    switch (true) {
      case Boolean(platform[1]):
        params.set("platform", platform[1].toLowerCase());
        hasParams = true;
        break;
      case Boolean(genre):
        params.set("genre", genre);
        hasParams = true;
        break;
      case Boolean(orderBy):
        params.set("orderBy", orderBy);
        hasParams = true;
        break;
      case Boolean(selectedReleaseDate):
        params.set("releaseDate", selectedReleaseDate);
        hasParams = true;
        break;
    }
    if (hasParams) {
      const url = `/?${params.toString()}`;
      navigate(url);
    }
  }, [platform, genre, orderBy, selectedReleaseDate]);

  const gamesToFilterAndSort =
    searchText !== ""
      ? searchedGames
      : gamesByGenreAndOrder.length !== 0
      ? gamesByGenreAndOrder
      : games;
  let filteredAndSortedGames = gamesToFilterAndSort.filter((game) => {
    const hasMatchingPlatform =
      platform[0] === true
        ? game.parent_platforms.some((item) =>
            item.platform.name.toLowerCase().includes(platform[1])
          )
        : platform[1] === "platforms" || platform.length === 0
        ? true
        : game.platforms.some(
            (item) => item.platform.name.toLowerCase() === platform[1]
          );

    const hasMatchingReleaseDate =
      selectedReleaseDate === "" ||
      game.released.includes(selectedReleaseDate) ||
      (typeof selectedReleaseDate !== "number" &&
        parseInt(game.released.split("-")[0]) >=
          parseInt(selectedReleaseDate.split("-")[0]) &&
        parseInt(game.released.split("-")[0]) <=
          parseInt(selectedReleaseDate.split("-")[1]));

    return hasMatchingPlatform && hasMatchingReleaseDate;
  });

  return (
    <Box width="80%" ml="22rem" mt="-46rem">
      <Container ml="-2rem" mt="2rem" mb="2rem">
        <Container
          width="60rem"
          color="white"
          fontSize="70px"
          fontWeight="bold"
        >
          New and trending
        </Container>
        <Container color="white" fontSize="20px">
          Based on players count and release date
        </Container>
      </Container>
      <Box>
        <Flex columnGap={2}>
          <Box position="relative" zIndex="2" width="10rem">
            {searchText === "" && <OrderBy />}
          </Box>
          <Box position="relative" zIndex="2" width="10rem">
            {genre && searchText === "" && <FilterByGenre />}
          </Box>
          <Box position="relative" zIndex="2" width="auto">
            {platform.length !== 0 && searchText === "" && <ReleaseDate />}
          </Box>
          <Box position="relative" zIndex="2" width="10rem">
            {searchText === "" && <FilterByPlatform />}
          </Box>
        </Flex>
      </Box>
      <Box>
        <Flex
          justifyContent="flex-end"
          mr="15rem"
          mb="1rem"
          alignItems="center"
          columnGap="2rem"
        >
          <Text fontSize="1.3rem" color="white" fontWeight="bold">
            Display options:
          </Text>
          <Button
            width="3.5rem"
            height="3.5rem"
            bgColor="gray.700"
            color="white"
            onClick={() => setDisplayStyle(true)}
            className="button1"
          ></Button>
          <Button
            width="3.5rem"
            height="3.5rem"
            bgColor="gray.700"
            color="white"
            onClick={() => setDisplayStyle(false)}
            className="button2"
          ></Button>
        </Flex>
      </Box>
      <Flex justifyContent="flex-end">
        {isError && (
          <Heading as="h3" color="white">
            {isError}
          </Heading>
        )}
        {isLoading && (
          <Heading as="h3" color="white">
            Loading...
          </Heading>
        )}
        {!isLoading &&
          games.length !== 0 &&
          filteredAndSortedGames.length === 0 && (
            <Heading color="white">No Matches Found</Heading>
          )}
        <Box width="100%" mr="3rem">
          <Flex flexWrap="wrap" columnGap="2rem">
            {filteredAndSortedGames.map((game, index) => (
              <Card
                key={index}
                width={displayStyle ? "20rem" : "50rem"}
                mt="2rem"
                ml={!displayStyle && "15rem"}
                marginBottom="2rem"
                boxShadow="md"
                borderRadius="1rem"
                bgColor="gray.800"
                onMouseEnter={() => setHoveredCardIndex(index)}
                onMouseLeave={() => setHoveredCardIndex(null)}
                position="relative"
                zIndex={hoveredCardIndex === index ? "2" : "1"}
                height={displayStyle ? "22rem" : "27rem"}
                transform={hoveredCardIndex === index ? "scale(1.05)" : "none"}
                transition="transform 0.3s ease"
              >
                <Image
                  src={game.background_image}
                  alt={game.name}
                  objectFit="fit"
                  height={displayStyle ? "12rem" : "18rem"}
                  borderTopRadius="1rem"
                />
                <Box p="4" position="relative" zIndex="2" className="buttons">
                  <Link to={`/games/detailed-page/${game.name}/${game.id}`}>
                    <Heading fontSize="1.2rem" mb="2" color="white">
                      {game.name}
                    </Heading>
                  </Link>
                  <Button
                    bgColor="gray.700"
                    color="white"
                    mt="2rem"
                    fontSize="1rem"
                  >
                    {game.added_by_status &&
                      `+ ${game.added_by_status.playing}`}
                  </Button>
                </Box>
                {hoveredCardIndex === index && (
                  <>
                    <Box
                      className="buttons"
                      position="absolute"
                      top={displayStyle ? "320px" : "410px"}
                      left="0px"
                      width="100%"
                      zIndex="2"
                      backgroundColor="gray.800"
                      mt={2}
                      pl={4}
                      pb={6}
                      pr={4}
                      borderBottomRadius="1rem"
                    >
                      <Button
                        ml="5rem"
                        mt={displayStyle ? "-6rem" : "-5.5rem"}
                        backgroundColor="gray.700"
                      >
                        <FaGift className="icons" />
                      </Button>
                      <Button
                        ml={2}
                        mt={displayStyle ? "-6rem" : "-5.5rem"}
                        backgroundColor="gray.700"
                      >
                        <BiDotsHorizontalRounded className="icons" />
                      </Button>
                      <Box mt={2}>
                        {" "}
                        <Platforms />
                      </Box>

                      <Text color="white" pl={2}>
                        Release date:{" "}
                        <Box as="span" ml="5rem">
                          {game.released}
                        </Box>
                      </Text>
                      <Divider mt={2} />
                      <Text mt={4} ml={2} color="white">
                        {game.genres && game.genres.length > 0
                          ? `Genres: ${game.genres
                              .map((i) => i.name)
                              .join(", ")}`
                          : "No genres"}
                      </Text>
                      <Divider mt={2} />
                      <Text mt={4} color="white">
                        Chart:
                      </Text>
                      <Button
                        width={displayStyle ? "17rem" : "35rem"}
                        mt={4}
                        ml={displayStyle ? 2 : 20}
                        backgroundColor="gray.700"
                        color="white"
                      >
                        Show more like this
                      </Button>
                      <Box as="br" />
                      <Button
                        width={displayStyle ? "17rem" : "35rem"}
                        mt={4}
                        ml={displayStyle ? 2 : 20}
                        backgroundColor="gray.700"
                        color="white"
                      >
                        Hide this game
                      </Button>
                    </Box>
                  </>
                )}
              </Card>
            ))}
          </Flex>
        </Box>
      </Flex>
    </Box>
  );
};

export default Games;
