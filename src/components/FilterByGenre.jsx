import React from "react";
import { Box, Menu, MenuItem, MenuButton, MenuList } from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { setGenre } from "../sliceFilesFolder/gamesSlice";

const FilterByGenre = () => {
  const dispatch = useDispatch();
  const genreName = useSelector((state) => state.games.genre);
  const genresData = useSelector((state) => state.games.genresData);
  const genres = genresData.map((item) => item.name);

  const handleSelectGenre = (selectedGenre) => {
    const newGenre = selectedGenre === "all" ? "" : selectedGenre.toLowerCase();
    dispatch(setGenre(newGenre));
  };

  return (
    <Box width="10rem" mb={4}>
      <Menu>
        <MenuButton
          color="white"
          border="1px solid white"
          borderRadius="md"
          p="2"
        >
          {genreName || "Genre"}
        </MenuButton>
        <MenuList
          position="absolute"
          left="5px"
          top="-50px"
          minWidth="120px"
          backgroundColor="white"
          boxShadow="md"
          py="2"
        >
          {genres.map((genre, index) => (
            <MenuItem key={index} onClick={() => handleSelectGenre(genre)}>
              {genre}
            </MenuItem>
          ))}
        </MenuList>
      </Menu>
    </Box>
  );
};

export default FilterByGenre;
