import React from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  setPlatform,
  setGenre,
  setOrderBy,
} from "../sliceFilesFolder/gamesSlice";
import { Box, Text, Heading, Icon } from "@chakra-ui/react";
import {
  FaWindows,
  FaXbox,
  FaPlaystation,
  FaApple,
  FaAndroid,
  FaGamepad,
} from "react-icons/fa";

const Sidebar = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const platformNames = [
    "PC",
    "PlayStation",
    "Xbox",
    "Nintendo",
    "iOS",
    "Android",
  ];

  const platformIcons = [
    FaWindows,
    FaPlaystation,
    FaXbox,
    FaGamepad,
    FaApple,
    FaAndroid,
  ];

  const genres = [
    "Action",
    "Strategy",
    "Shooter",
    "Adventure",
    "Puzzle",
    "Racing",
    "Sports",
  ];

  const handlePlatformClick = (platformName) => {
    dispatch(setPlatform([true, platformName.toLowerCase()]));
  };

  const handleGenreClick = (genre) => {
    dispatch(setGenre(genre.toLowerCase()));
  };
  const handleHomeClick = () => {
    dispatch(setPlatform([]));
    dispatch(setGenre(""));
    dispatch(setOrderBy(""));
    navigate("/");
  };

  return (
    <Box mt="3rem" ml="6rem">
      <Heading
        mt="2rem"
        color="white"
        cursor="pointer"
        onClick={handleHomeClick}
      >
        Home
      </Heading>
      <Box>
        <Heading color="white" mt="1rem" mb="1rem">
          Platforms
        </Heading>
        {platformNames.map((platform, index) => {
          const PlatformIcon = platformIcons[index];
          return (
            <Text
              key={platform}
              className="platform"
              color="white"
              fontSize="1.5rem"
              cursor="pointer"
              onClick={() => handlePlatformClick(platform)}
            >
              <Icon
                as={PlatformIcon}
                className="icon"
                borderRadius={2}
                color="white"
                boxSize={5}
                mr={2}
              />
              {platform}
            </Text>
          );
        })}
      </Box>
      <Box width="90%">
        <Heading color="white" mt="1rem" mb="1rem">
          Genres
        </Heading>
        {genres.map((genre) => {
          return (
            <Text
              key={genre}
              color="white"
              fontSize="1.5rem"
              cursor="pointer"
              onClick={() => handleGenreClick(genre)}
            >
              {genre}
            </Text>
          );
        })}
      </Box>
    </Box>
  );
};

export default Sidebar;
