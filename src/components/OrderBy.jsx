import React, { useState } from "react";
import { Box, Menu, MenuItem, MenuButton, MenuList } from "@chakra-ui/react";
import { useDispatch } from "react-redux";
import { setOrderBy } from "../sliceFilesFolder/gamesSlice";

const OrderBy = () => {
  const dispatch = useDispatch();
  const [selectedOrder, setSelectedOrder] = useState("");
  const options = ["Name", "Release date", "Popularity", "Rating"];

  const handleSelectedOrder = (selectedOrder) => {
    dispatch(setOrderBy(selectedOrder));
    setSelectedOrder(selectedOrder);
  };

  return (
    <Box>
      <Menu>
        <MenuButton
          width="8rem"
          color="white"
          border="1px solid white"
          borderRadius="md"
          p="2"
        >
          {selectedOrder ? selectedOrder : "Order By"}
        </MenuButton>
        <MenuList
          position="absolute"
          left="5px"
          top="-45px"
          minWidth="140px"
          backgroundColor="white"
          boxShadow="md"
          py="2"
        >
          {options.map((option, index) => (
            <MenuItem
              key={index}
              onClick={() => handleSelectedOrder(option.toLowerCase())}
            >
              {option}
            </MenuItem>
          ))}
        </MenuList>
      </Menu>
    </Box>
  );
};

export default OrderBy;
