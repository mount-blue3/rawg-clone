import React, { useState } from "react";
import {
  Input,
  InputGroup,
  InputLeftElement,
  InputRightElement,
} from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import { SearchIcon } from "@chakra-ui/icons";
import { useDispatch } from "react-redux";
import { setSearchText } from "../sliceFilesFolder/gamesSlice";

const Search = () => {
  const dispatch = useDispatch();
  const [searchValue, setSearchValue] = useState("");

  const handleInputChange = (event) => {
    const searchText = event.target.value;
    setSearchValue(searchText);
    handleSearch();
  };

  const handleSearch = () => {
    dispatch(setSearchText(searchValue));
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      handleSearch();
    }
  };
  const handleCancel = () => {
    setSearchValue("");
    dispatch(setSearchText(""));
  };

  return (
    <InputGroup width="80rem">
      <Input
        type="text"
        placeholder="Search 850,295 games"
        borderRadius="2rem"
        height="3rem"
        bgColor="grey.700"
        color="white"
        className="search-bar"
        value={searchValue}
        onChange={handleInputChange}
        onKeyDown={handleKeyDown}
      />
      <InputLeftElement
        children={
          <SearchIcon className="search-icon" color="grey" mt={2} ml={1} />
        }
      />
      <InputRightElement
        children={
          <CloseIcon ml="-5rem" color="grey" mt={1} onClick={handleCancel} />
        }
      />
    </InputGroup>
  );
};

export default Search;
