import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { ChevronRightIcon } from "@chakra-ui/icons";
import { setSelectedReleaseDate } from "../sliceFilesFolder/gamesSlice";
import {
  Box,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
  MenuDivider,
} from "@chakra-ui/react";

const ReleaseDate = () => {
  const dispatch = useDispatch();
  const [selectedOption, setSelectedOption] = useState("");
  const [selectedMainOption, setSelectedMainOption] = useState("");

  const options = [
    "2020-2023",
    "2010-2019",
    "2000-2009",
    "1990-1999",
    "1980-1989",
    "1970-1979",
  ];

  const handleOptionClick = (option) => {
    if (option === "select all") {
      setSelectedOption(selectedMainOption);
      dispatch(setSelectedReleaseDate(selectedMainOption));
    } else {
      setSelectedOption(option);
      dispatch(setSelectedReleaseDate(option));
    }
  };

  const renderNestedOptions = () => {
    if (!selectedMainOption) return null;

    const [startYear, endYear] = selectedMainOption.split("-");

    const nestedOptions = [];
    for (let year = parseInt(endYear); year >= parseInt(startYear); year--) {
      nestedOptions.push(
        <MenuItem
          key={year}
          value={year}
          onClick={() => handleOptionClick(year)}
        >
          {year}
        </MenuItem>
      );
    }
    nestedOptions.push(
      <>
        <MenuDivider />
        <MenuItem
          key="select-all"
          value={selectedMainOption}
          color="green"
          onClick={() => handleOptionClick("select all")}
        >
          Select all
        </MenuItem>
      </>
    );

    return nestedOptions;
  };

  return (
    <Box width="auto">
      <Menu>
        <MenuButton
          color="white"
          border="1px solid white"
          borderRadius="md"
          p="2"
        >
          {selectedOption ? `Release Date:${selectedOption}` : "Release Date"}
        </MenuButton>
        <MenuList
          position="absolute"
          top="-50px"
          backgroundColor="white"
          minWidth="140px"
          boxShadow="md"
          py="2"
        >
          {options.map((option, index) => (
            <MenuItem
              key={index}
              value={option}
              onMouseEnter={() => setSelectedMainOption(option)}
              onMouseLeave={() => setSelectedMainOption("")}
            >
              {option}
              <ChevronRightIcon />
              {selectedMainOption === option && (
                <MenuList
                  position="absolute"
                  left="100px"
                  top="20px"
                  backgroundColor="white"
                  minWidth="120px"
                  boxShadow="md"
                  py="2"
                >
                  {renderNestedOptions()}
                </MenuList>
              )}
            </MenuItem>
          ))}
        </MenuList>
      </Menu>
    </Box>
  );
};

export default ReleaseDate;
