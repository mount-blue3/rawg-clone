import React, { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { FiFolder, FiCircle } from "react-icons/fi";
import { FaComment } from "react-icons/fa";
import { AiOutlinePlusCircle } from "react-icons/ai";
import {
  Box,
  Text,
  Flex,
  Heading,
  Button,
  Container,
  Icon,
  Image,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { setPlatform, setGenre } from "../sliceFilesFolder/gamesSlice";
import { fetchGameDetails } from "../sliceFilesFolder/detailedGamePageSlice";
import Platforms from "./Platforms";
import { EditIcon } from "@chakra-ui/icons";

function DetailedGamePage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const gameDetails = useSelector((state) => state.gameDetails.gameDetails);
  const gameSeries = useSelector((state) => state.gameDetails.gameSeries);
  const dlcsAndEditions = useSelector(
    (state) => state.gameDetails.dlcsAndEditions
  );

  const params = useParams();
  useEffect(() => {
    dispatch(fetchGameDetails(params.gameID));
  }, []);

  const ratingsCount = (name) => {
    const result = gameDetails.ratings.map((item) => {
      if (item.title === name) {
        return item.count;
      }
    });
    return result;
  };
  const releaseDate = (date) => {
    const month = [
      "JAN",
      "FEB",
      "MAR",
      "APR",
      "MAY",
      "JUN",
      "JUL",
      "AUG",
      "SEP",
      "OCT",
      "NOV",
      "DEC",
    ];
    const temp = date.split("-");
    const displayDate = `${month[parseInt([temp[1]])]} ${temp[2]} ${temp[0]}`;
    return displayDate;
  };

  const handleHomeAndGamesClick = () => {
    dispatch(setPlatform([]));
    dispatch(setGenre(""));
    navigate("/");
  };

  return (
    <Box width="80%" ml="22rem" mt="-43rem" color="white">
      <Flex>
        <Box width="50%" ml="5rem">
          <Box color="grey" mb="2rem">
            <Flex>
              <Text onClick={handleHomeAndGamesClick} cursor="pointer">
                HOME /
              </Text>
              <Text onClick={handleHomeAndGamesClick} cursor="pointer">
                GAMES /
              </Text>
              {gameDetails.name && <Text ml={1}> {gameDetails.name}</Text>}
            </Flex>
          </Box>
          <Box>
            <Flex>
              {gameDetails.released && (
                <Button height="1.5rem" mr="1rem">
                  {releaseDate(gameDetails.released)}
                </Button>
              )}
              <Box mt="2rem" mr={2}>
                <Platforms />
              </Box>
              {gameDetails.playtime && (
                <Text fontWeight="bold">
                  AVERAGE PLAYTIME: {gameDetails.playtime} hours
                </Text>
              )}
            </Flex>
          </Box>
          {gameDetails.name && (
            <Heading fontSize="4rem" mb="2rem">
              {gameDetails.name}
            </Heading>
          )}
          <Box>
            <Button width="10rem" height="3rem" mr={2} pb={1} pt={1}>
              <Flex direction="column">
                <Text color="grey" ml="-5rem" mb={-1}>
                  Add to
                </Text>
                <Text fontWeight="normal" ml="-3.5rem">
                  My games
                </Text>
              </Flex>
            </Button>
            <Button bgColor="transparent" _hover={{ bgColor: "transparent" }}>
              <Flex direction="column">
                <Text color="grey">Add to</Text>
                <Text color="white" ml={2}>
                  Wishlist
                </Text>
              </Flex>
            </Button>
            <Button bgColor="transparent" _hover={{ bgColor: "transparent" }}>
              <Flex direction="column">
                <Text color="grey">Save to</Text>
                <Text color="white" ml={4}>
                  Collection{" "}
                  <Box ml="7rem" mt="-1.2rem">
                    <FiFolder />
                  </Box>
                </Text>
              </Flex>
            </Button>
          </Box>
          <Box mt="3rem">
            {gameDetails.ratings_count && (
              <Text color="grey"> {gameDetails.ratings_count} RATINGS</Text>
            )}
            <Text></Text>
          </Box>
          <Box mt={4}>
            <Text>Click to rate</Text>
            <Button
              width="18rem"
              borderRadius={0}
              bgColor="green.400"
              _hover={{
                bgColor: "green.400",
                transform: "scale(1.1)",
                zIndex: 1,
              }}
            ></Button>
            <Button
              width="8rem"
              borderRadius={0}
              bgColor="blue"
              _hover={{ bgColor: "blue", transform: "scale(1.1)", zIndex: 1 }}
            ></Button>
            <Button
              width="3rem"
              borderRadius={0}
              bgColor="orange"
              _hover={{ bgColor: "orange", transform: "scale(1.1)", zIndex: 1 }}
            ></Button>
            <Button
              width="0.5rem"
              borderRadius={0}
              bgColor="red"
              _hover={{ bgColor: "red", transform: "scale(1.1)", zIndex: 1 }}
            ></Button>
          </Box>
          {gameDetails.ratings && (
            <Box mt={4}>
              <Flex columnGap={2}>
                <Flex alignItems="center">
                  <Box mr={2}>
                    <Icon
                      as={FiCircle}
                      color="green.500"
                      boxSize={3}
                      borderRadius="full"
                      bg="green.500"
                    />
                  </Box>
                  <Flex>
                    <Text fontWeight="bold">Exceptional </Text>
                    <Text color="grey" fontWeight="bold" ml={3}>
                      {ratingsCount("exceptional")}
                    </Text>
                  </Flex>
                </Flex>
                <Flex alignItems="center">
                  <Box mr={2}>
                    <Icon
                      as={FiCircle}
                      color="blue"
                      boxSize={3}
                      borderRadius="full"
                      bg="blue"
                    />
                  </Box>
                  <Flex>
                    <Text fontWeight="bold">Recommended </Text>
                    <Text color="grey" fontWeight="bold" ml={3}>
                      {ratingsCount("recommended")}
                    </Text>
                  </Flex>
                </Flex>
                <Flex alignItems="center">
                  <Box mr={2}>
                    <Icon
                      as={FiCircle}
                      color="orange"
                      boxSize={3}
                      borderRadius="full"
                      bg="orange"
                    />
                  </Box>
                  <Flex>
                    <Text fontWeight="bold">Meh </Text>
                    <Text color="grey" fontWeight="bold" ml={3}>
                      {ratingsCount("meh")}
                    </Text>
                  </Flex>
                </Flex>
              </Flex>
              <Flex alignItems="center">
                <Box mr={2}>
                  <Icon
                    as={FiCircle}
                    color="red"
                    boxSize={3}
                    borderRadius="full"
                    bg="red"
                  />
                </Box>
                <Flex>
                  <Text fontWeight="bold">Skip </Text>
                  <Text color="grey" fontWeight="bold" ml={3}>
                    {ratingsCount("skip")}
                  </Text>
                </Flex>
              </Flex>
            </Box>
          )}
          <Box mt={6}>
            <Button
              bgColor="gray.800"
              color="gray"
              mr={10}
              _hover={{ bgColor: "transparent", color: "white" }}
            >
              <Icon as={AiOutlinePlusCircle} boxSize={5} mr={1} />
              Write a review
            </Button>
            <Button
              bgColor="gray.800"
              color="gray"
              _hover={{ bgColor: "transparent", color: "white" }}
            >
              <Icon as={FaComment} boxSize={6} mr={1} />
              Write a comment
            </Button>
          </Box>
          {gameDetails.description_raw && (
            <Box mt={6}>
              <Heading fontSize="1.5rem">About</Heading>
              <Container mt={3} ml="-1.5rem" fontWeight="bold">
                {gameDetails.description_raw}
              </Container>
            </Box>
          )}
          <Box>
            <Flex alignItems="center" columnGap={4}>
              {gameDetails.platforms &&
                Array.isArray(gameDetails.platforms) && (
                  <Box mt={8} width="20rem">
                    <Heading fontSize="1rem" color="gray">
                      Platforms
                    </Heading>
                    <Text
                      mt={2}
                      fontWeight="bold"
                      _hover={{ color: "gray" }}
                      textDecoration="underline"
                      cursor="pointer"
                    >
                      {gameDetails.platforms
                        .map((item) => item.platform.name)
                        .join(", ")}
                    </Text>
                  </Box>
                )}
              <Box>
                <Heading fontSize="1rem" color="gray">
                  MetaScore
                </Heading>
                <Box
                  width="2rem"
                  ml={6}
                  mt={2}
                  pl={1}
                  borderWidth={2}
                  borderColor="green"
                  color="green"
                  fontWeight="bold"
                >
                  {gameDetails.metacritic}
                </Box>
              </Box>
            </Flex>
          </Box>
          <Box mt={6}>
            <Flex alignItems="center" columnGap="1.5rem">
              {gameDetails.genres && Array.isArray(gameDetails.genres) && (
                <Box width="20rem">
                  <Heading fontSize="1rem" color="gray">
                    Genre
                  </Heading>
                  <Text
                    fontWeight="bold"
                    mt={2}
                    _hover={{ color: "gray" }}
                    textDecoration="underline"
                    cursor="pointer"
                  >
                    {gameDetails.genres.map((item) => item.name).join(", ")}
                  </Text>
                </Box>
              )}
              {gameDetails.released && (
                <Box>
                  <Heading fontSize="1rem" color="gray">
                    Release Date
                  </Heading>
                  <Text mt={2} fontWeight="bold">
                    {releaseDate(gameDetails.released)}
                  </Text>
                </Box>
              )}
            </Flex>
          </Box>
          <Box mt={6}>
            <Flex alignItems="center" columnGap="1.5rem">
              {gameDetails.developers &&
                Array.isArray(gameDetails.developers) && (
                  <Box width="20rem">
                    <Heading fontSize="1rem" color="gray">
                      Developer
                    </Heading>
                    <Text
                      mt={2}
                      fontWeight="bold"
                      _hover={{ color: "gray" }}
                      textDecoration="underline"
                      cursor="pointer"
                    >
                      {gameDetails.developers
                        .map((item) => item.name)
                        .join(", ")}
                    </Text>
                  </Box>
                )}
              {gameDetails.publishers &&
                Array.isArray(gameDetails.publishers) && (
                  <Box>
                    <Heading fontSize="1rem" color="gray">
                      Publisher
                    </Heading>
                    <Text
                      mt={2}
                      fontWeight="bold"
                      _hover={{ color: "gray" }}
                      textDecoration="underline"
                      cursor="pointer"
                    >
                      {gameDetails.publishers
                        .map((item) => item.name)
                        .join(", ")}
                    </Text>
                  </Box>
                )}
            </Flex>
          </Box>
          <Box mt={6}>
            <Flex alignItems="center" columnGap="2rem">
              <Heading fontSize="1rem" color="gray">
                Age rating
              </Heading>
              {gameDetails.esrb_rating && (
                <Text color="white" fontWeight="bold">
                  {gameDetails.esrb_rating.name}
                </Text>
              )}
            </Flex>
          </Box>
          <Box mt={6} width="25rem">
            <Flex direction="column">
              <Heading fontSize="1rem" color="gray">
                other games in the series
              </Heading>
              {gameSeries && Array.isArray(gameSeries) && (
                <Text
                  color="white"
                  fontWeight="bold"
                  mt={2}
                  _hover={{ color: "gray" }}
                  textDecoration="underline"
                  cursor="pointer"
                >
                  {gameSeries.map((item) => item.name).join(", ")}
                </Text>
              )}
            </Flex>
          </Box>
          <Box mt={6} width="25rem">
            <Flex direction="column">
              <Heading fontSize="1rem" color="gray">
                DLC's and editions
              </Heading>
              {dlcsAndEditions && Array.isArray(dlcsAndEditions) && (
                <Text
                  color="white"
                  fontWeight="bold"
                  mt={2}
                  _hover={{ color: "gray" }}
                  textDecoration="underline"
                  cursor="pointer"
                >
                  {dlcsAndEditions.map((item) => item.name).join(", ")}
                </Text>
              )}
            </Flex>
          </Box>
          <Box mt={6} width="25rem">
            <Heading fontSize="1rem" color="gray">
              Tags
            </Heading>
            {gameDetails.tags && Array.isArray(gameDetails.tags) && (
              <Text
                fontWeight="bold"
                mt={2}
                _hover={{ color: "gray" }}
                textDecoration="underline"
              >
                {gameDetails.tags.map((item) => item.name).join(", ")}
              </Text>
            )}
          </Box>
          <Box mt={6} mb="10rem">
            <Heading fontSize="1rem" color="gray">
              Website
            </Heading>
            <Text
              fontWeight="bold"
              mt={2}
              _hover={{ color: "gray" }}
              textDecoration="underline"
            >
              {gameDetails.website}
            </Text>
          </Box>
        </Box>
        <Box width="48%" mt={6}>
          {gameDetails.background_image && (
            <Box>
              <Image
                width="20rem"
                src={`${gameDetails.background_image}`}
              ></Image>
              <Button mt={4} width="20rem">
                <Icon as={EditIcon} mr={2} />
                Edit the Game info
              </Button>
              <Text color="gray" mt={2}>
                Last Modified:{gameDetails.updated.slice(0, 10)}
              </Text>
            </Box>
          )}
          {gameDetails.stores && Array.isArray(gameDetails.stores) && (
            <Box mt={6}>
              <Text fontSize="1.5rem" color="gray" fontWeight="bold">
                Where to buy
              </Text>
              <Box width="20rem">
                {gameDetails.stores.map((item) => {
                  return (
                    <Button mt={2} ml={2}>
                      {item.store.name}
                    </Button>
                  );
                })}
              </Box>
            </Box>
          )}
          {/* <Box mt={6}>
            <Text color="gray" fontSize="1.5rem">
              Top contributors
            </Text>
            <Text></Text>
          </Box>
          <Box mt={6}>
            <Heading fontSize="1.5rem">{`Collections with ${gameDetails.name}`}</Heading>
          </Box> */}
        </Box>
      </Flex>
    </Box>
  );
}

export default DetailedGamePage;
