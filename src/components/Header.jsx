import React from 'react';
import { Container } from '@chakra-ui/react';
import Search from './Search';

const Header = () => {
  return (
    <Container className="header-container">
      <Container className="name">R A W G</Container>
      <Container mt="4rem" ml="-50rem">
        <Search />
      </Container>
    </Container>
  );
};

export default Header;